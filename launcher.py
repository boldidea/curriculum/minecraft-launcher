#!/usr/bin/env python3
import argparse
import hashlib
import json
import os
import platform
import re
import shutil
import sys
import tkinter as tk
from tkinter import ttk
from pathlib import Path
import urllib.error
import urllib.parse
import urllib.request
from contextlib import contextmanager
from zipfile import ZipFile, BadZipfile     # JARs are secretly ZIP files.

HOME_DIR = str(Path.home())
default_data_dir = os.path.join(HOME_DIR, '.minecraft')
default_minecraft_version='1.11'

parser = argparse.ArgumentParser()
parser.add_argument(
    '-u', '--update', help="update only, don't launch", dest='update_only', action='store_true')
parser.add_argument(
    '-d', '--data-dir', help='data directory path', nargs='?', default=default_data_dir)
parser.add_argument(
    '-v', '--version', help='minecraft version', nargs='?')
parser.add_argument(
    '-n', '--username', help='minecraft username', nargs='?')
args = parser.parse_args()

DATA_DIR = args.data_dir
VERSION_MANIFEST_URL = 'https://launchermeta.mojang.com/mc/game/version_manifest.json'
VERSION_MANIFEST_FILE = os.path.join(DATA_DIR, 'version_manifest.json')
ASSETS_URL = 'http://resources.download.minecraft.net/'

# text modes
BOLD = '\033[1m'
HEADER = '\033[95m'
INFO = '\033[94m'
OK = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'


def detect_mc_version():
    # Attempt to guess latest installed minecraft version
    versions_dir = os.path.join(DATA_DIR, 'versions')
    if os.path.exists(versions_dir):
        versions = [f for f in os.listdir(versions_dir) if not f.endswith('.jar')]
        if len(versions) > 0:
            versions.sort(key=lambda s: list(map(int, s.split('.'))))
            return versions[-1]
    return None


def detect_platform():
    os = None
    if sys.platform.startswith('win'):
        os = 'windows'
    elif sys.platform == 'darwin':
        os = 'osx'
    else:
        os = 'linux'

    bits = '32'
    if platform.architecture() == '64bit':
        bits = '64'

    return os, bits


def log(msg, mode='', out=None):
    out = out or sys.stdout
    print(mode + msg + '\033[0m', file=out)


def abort(msg):
    log(msg, FAIL, sys.stderr)
    sys.exit(1)


def chunks(f, chunk_size):
    data = f.read(chunk_size)
    while data:
        yield data
        data = f.read(chunk_size)


def file_is_valid(path, fingerprint):
    if not os.path.exists(path):
        return False

    # check sha1 fingerprint of file
    sha1 = hashlib.sha1()
    with open(path, 'rb') as f:
        # read file in 64kb chunks
        for data in chunks(f, 65536):
            sha1.update(data)

    return hash != sha1.hexdigest()


@contextmanager
def progress(count, total, status=''):
    try:
        yield
        count += 1
        bar_len = 60
        filled_len = int(round(bar_len * count / float(total)))
        percent = round(100.0 * count / float(total), 1)
        bar = '=' * filled_len + '-' * (bar_len - filled_len)
        sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percent, '%', status))
        if percent >= 100:
            sys.stdout.write('\n')
        sys.stdout.flush()

    except Exception as e:
        sys.stdout.flush()
        sys.stdout.write('\n')
        raise


def get_username():
    def on_click(*args):
        value = e1.get()
        data[0] = value
        root.destroy()

    saved_username = ''
    username_file_path = os.path.join(HOME_DIR, '.minecraft-username')
    if os.path.exists(username_file_path):
        with open(username_file_path) as username_file:
            saved_username = username_file.read().strip()

    data = ['']
    root = tk.Tk()
    root.title('Minecraft Launcher')
    root.bind('<Escape>', lambda e: root.destroy())
    ttk.Style(root).configure('padded.TEntry', padding=[5, 5, 5, 5])
    e1 = ttk.Entry(root, style='padded.TEntry')
    e1.insert(tk.END, saved_username)
    e1.grid(row=0, column=1, padx=10, pady=10)
    e1.bind('<Return>', on_click)
    e1.focus()
    tk.Label(root, text="Username:").grid(row=0, sticky=tk.W, padx=10, pady=10)
    btn = tk.Button(root, text='Launch Minecraft', command=on_click)
    btn.grid(row=1, columnspan=2, sticky=tk.E, pady=10, padx=10)
    tk.mainloop()

    with open(username_file_path, 'w') as username_file:
        username_file.write(data[0])

    return data[0]


class Profile:
    def __init__(self, version):
        self._version_manifest = None
        self._assets_index = None
        self._version_info = {}
        self.version = version
        self.libpaths = []
        self.libraries = []
        self.main_class = self.version_info['mainClass']
        self.mc_args = self.version_info['minecraftArguments']
        self.jar = os.path.join('versions', self.version + '.jar')

        self.os, self.bits = detect_platform()

        # Populate needed libraries
        for libinfo in self.version_info['libraries']:
            if not self.rules_satisified(libinfo.get('rules')):
                continue

            downloads = libinfo.get('downloads')
            name = libinfo['name']
            artifacts = []
            natives = []

            if downloads.get('artifact'):
                fileinfo = downloads['artifact']
                artifacts.append(fileinfo)
                self.libpaths.append(os.path.join('libraries', fileinfo['path']))

            if 'natives' in libinfo and self.os in libinfo['natives']:
                classifier = libinfo['natives'][self.os].replace('${arch}', self.bits)
                fileinfo = downloads['classifiers'][classifier]
                natives.append(fileinfo)
                self.libpaths.append(os.path.join('libraries', fileinfo['path']))

            self.libraries.append({
                'name': name,
                'artifacts': artifacts,
                'natives': natives
            })

    def rule_applies(self, rule):
        """
        Return true if library rule applies to the current platform

        https://github.com/tomsik68/mclauncher-api/wiki/Rules-List
        """
        os_rule = rule.get('os')
        if os_rule is None:
            return True

        if os_rule['name'] != self.os:
            return False

        os_version = os_rule.get('version')
        if os_version is None:
            return True

        if re.match(os_version, platform.version()):
            return True

        return False

    def rules_satisified(self, rules):
        """
        Check to see if a rules set is satisifed for this platform

        https://github.com/tomsik68/mclauncher-api/wiki/Rules-List
        """
        if rules is None:
            return True

        for rule in rules:
            if rule['action'] == 'allow' and not self.rule_applies(rule):
                return False
            if rule['action'] == 'disallow' and self.rule_applies(rule):
                return False

        return True

    def update_version_manifest(self):
        log('Fetching latest version manifest...', HEADER)
        self.download_file(VERSION_MANIFEST_FILE, VERSION_MANIFEST_URL)
        with open(VERSION_MANIFEST_FILE) as f:
            self._version_manifest = json.load(f)
            self._assets_index = None

    @property
    def version_manifest(self):
        if not self._version_manifest:
            if not os.path.exists(VERSION_MANIFEST_FILE):
                self.update_version_manifest()
                return self._version_manifest
            with open(VERSION_MANIFEST_FILE) as f:
                self._version_manifest = json.load(f)
        return self._version_manifest

    @property
    def assets_index(self):
        if not self._assets_index:
            assets_name = self.version_info.get('assets', 'legacy')
            assets_index_file = os.path.join(DATA_DIR, 'assets', 'indexes', assets_name + '.json')
            if not os.path.exists(assets_index_file):
                self.download_file(assets_index_file, self.version_info['assetIndex']['url'])
            with open(assets_index_file) as f:
                self._assets_index = json.load(f)
        return self._assets_index

    def find_version(self):
        for info in self.version_manifest['versions']:
            if info['id'] == self.version:
                return info
        return None

    @property
    def version_info(self):
        # If the version isn't found in the manifest, try updating
        vinfo = self.find_version()
        if not vinfo:
            self.update_version_manifest()
            vinfo = self.find_version()
            if not vinfo:
                abort('Invalid version: ' + self.version)

        if not self._version_info:
            version_file = os.path.join(DATA_DIR, 'versions', self.version, self.version + '.json')
            if not os.path.isfile(version_file):
                self.download_file(version_file, vinfo['url'])
            with open(version_file) as f:
                self._version_info = json.load(f)

        return self._version_info

    def download_file(self, path, url, sha1=None, silent=False):
        if not silent:
            log('> Downloading ' + path, INFO)
        dirname = os.path.dirname(path)
        os.makedirs(dirname, exist_ok=True)

        try:
            with urllib.request.urlopen(url) as response, open(path, 'wb') as outf:
                shutil.copyfileobj(response, outf)
        except urllib.error.HTTPError as e:
            abort('Could not download %s\n%s' % (url, str(e)))

        if sha1 and not file_is_valid(path, sha1):
            abort('File is corrupt: %s' % path)

    def update(self):
        os.makedirs(DATA_DIR, exist_ok=True)
        with open(os.path.join(DATA_DIR, 'options.txt'), 'w') as options_file:
            options_file.write('pauseOnLostFocus:false\n')

        # Client .jar file
        log('Downloading client...', HEADER)
        client_path = os.path.join(DATA_DIR, self.jar)
        if not os.path.exists(client_path):
            fileinfo = self.version_info['downloads']['client']
            self.download_file(client_path, fileinfo['url'], sha1=fileinfo['sha1'])

        # Collect libraries to download
        log('Updating libraries...', HEADER)
        natives_dir = os.path.join(DATA_DIR, 'natives')
        library_downloads = []
        asset_downloads = []
        natives = []
        for libinfo in self.libraries:
            for fileinfo in libinfo['artifacts']:
                filepath = os.path.join(DATA_DIR, 'libraries', fileinfo['path'])
                if not file_is_valid(filepath, fileinfo['sha1']):
                    library_downloads.append((filepath, fileinfo['url'], fileinfo['sha1']))

            for fileinfo in libinfo['natives']:
                filepath = os.path.join(DATA_DIR, 'libraries', fileinfo['path'])
                natives.append(filepath)
                if not file_is_valid(filepath, fileinfo['sha1']):
                    library_downloads.append((filepath, fileinfo['url'], fileinfo['sha1']))

        total = len(library_downloads)
        for i, (filepath, url, sha1) in enumerate(library_downloads):
            with progress(i, total):
                self.download_file(filepath, url, sha1, silent=True)

        log('Updating assets...', HEADER)
        # Collect assets to download
        for name, asset_info in self.assets_index['objects'].items():
            hash = asset_info['hash']
            prefix = hash[:2]
            filepath = os.path.join(DATA_DIR, 'assets', 'objects', prefix, hash)
            if not file_is_valid(filepath, hash):
                asset_url = ASSETS_URL + prefix + '/' + hash
                asset_downloads.append((filepath, asset_url, hash))

        total = len(asset_downloads)
        for i, (filepath, url, sha1) in enumerate(asset_downloads):
            with progress(i, total):
                self.download_file(filepath, url, sha1, silent=True)

        log('Extracting native libraries...', HEADER)
        total = len(natives)
        for i, filepath in enumerate(natives):
            # extract native files from the .jar
            try:
                zipfile = ZipFile(filepath, 'r')
                for name in zipfile.namelist():
                    if name.startswith("META-INF") or name.startswith("."):
                        continue
                    if not os.path.exists(os.path.join(natives_dir, name)):
                        zipfile.extract(name, natives_dir)
                zipfile.close()
            except BadZipfile:
                os.remove(filepath)
                abort('Bad zip file: %s' % filepath)

    def launch(self, username):
        libs = [self.jar]
        libs.extend(self.libpaths)

        if self.os == 'windows':
            classpath = ';'.join(libs)
        else:
            classpath = ':'.join(libs)

        # TODO figure out optimal jvm args for chromebooks?
        # https://www.minecraftforum.net/forums/support/java-edition-support/2882324-java-jvm-arguments-for-minecraft-java-edition
        jvm_args = ''

        # Build minecraft arguments using the template string we got from the manifest
        mc_args = self.mc_args
        mc_args_map = {
            'auth_player_name': username,
            'version_type': 'release',
            'version_name': self.version,
            'game_directory': '.',
            'game_assets': 'assets',
            'assets_root': 'assets',
            'auth_uuid': 'null',
            'auth_access_token': 'null',
            'assets_index_name': self.version_info.get('assets', 'legacy'),
            'user_properties': '{}',
            'user_type': 'offline'
        }
        for key, val in mc_args_map.items():
            mc_args = mc_args.replace('${' + key + '}', val)

        # Build the full launch command
        cmd = 'java {jvm_args} -cp "{classpath}" -Djava.library.path=natives {main} {mc_args}'
        cmd = cmd.format(
            jvm_args=jvm_args,
            classpath=classpath,
            main=self.main_class,
            mc_args=mc_args
        )

        # Run it!
        log('Starting Minecraft...', HEADER)
        os.system('cd ' + DATA_DIR + ' && ' + cmd)


if __name__ == '__main__':
    # Launch the profile
    version = args.version or detect_mc_version() or default_minecraft_version
    log('Using version {}'.format(version), INFO)
    p = Profile(version)
    p.update()

    if not args.update_only:
        username = args.username or get_username()
        if not username:
            sys.exit(0)
        username = re.sub(r'[^0-9A-Za-z_]', '', username[0].upper() + username[1:])

        p.launch(username)
